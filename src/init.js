import fs from 'fs';
import db from './services/db.js';
import { seeds } from './seeds.js';
import { dbRedisConfig } from './services/redis.js';
import { dbElasticSearchConfig } from './services/elasticSearch.js';

const postgresInit = async () => {
  try {
    await db.sequelize.authenticate();
  } catch (err) {
    console.log('DB error___', err);
  }
};
const redisInit = async () => {
  try {
    await dbRedisConfig();
  } catch (err) {
    console.log('redis error___', err);
  }
};

const elasticInit = async () => {
  try {
    await dbElasticSearchConfig();
  } catch (err) {
    console.log('elastic error___', err);
  }
};

const seedsInit = async () => {
  try {
    await seeds();
  } catch (err) {
    console.log('seed error', err);
  }
};

const directoryInit = () => {
  if (!fs.existsSync('uploads')) {
    fs.mkdirSync('uploads', { recursive: true });
  }
};

export const initProject = async () => {
  await Promise.all([
    postgresInit(),
    redisInit(),
    elasticInit()]);

  await seedsInit();

  directoryInit();

  console.log('Project is ready');
};
