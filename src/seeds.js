import { createRoleService, getRoleByNameService } from './api/role/role.service.js';
import { createAdminService, getUserByRoleIdService } from './api/user/user.service.js';
import { createCategoryService, getCategoryByNameService } from './api/category/category.service.js';

const defaultRoleSeed = async () => {
  const [superAdmin, client, admin] = await Promise.all([
    getRoleByNameService('SUPER ADMIN'),
    getRoleByNameService('CLIENT'),
    getRoleByNameService('ADMIN')]);
  if (superAdmin == null) {
    await createRoleService({ name: 'SUPER ADMIN' });
  }
  if (client == null) {
    await createRoleService({ name: 'CLIENT' });
  }
  if (admin == null) {
    await createRoleService({ name: 'ADMIN' });
  }
};

const defaultAdminSeed = async () => {
  const role = await getRoleByNameService('SUPER ADMIN', ['id']);
  const user = await getUserByRoleIdService(role.id, ['id']);

  if (user == null) {
    await createAdminService({
      email: process.env.ADMIN_EMAIL,
      firstName: 'Admin',
      lastName: 'Admin',
      isConfirmed: true,
      password: process.env.ADMIN_PASSWORD,
    }, role.id);
  }
};

const defaultCategorySeed = async () => {
  const category = await getCategoryByNameService('HEAD');

  if (category == null) {
    await createCategoryService({
      name: 'HEAD',
      description: 'Default category',
    });
  }
};

export const seeds = async () => {
  await defaultRoleSeed();
  await Promise.all([
    defaultAdminSeed(),
    defaultCategorySeed(),
  ]);
};
