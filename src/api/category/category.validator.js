import { body, param, query } from 'express-validator';
import { GENERAL_ERRORS } from '../../utils/error-messages.js';
import { validationResultMiddleware } from '../../middlewares/validation-result.js';

export const getAllValidator = [
  query('from').toInt().notEmpty().withMessage(GENERAL_ERRORS.isRequired('From'))
    .isInt({ min: 0, max: 2147483647 })
    .withMessage(GENERAL_ERRORS.isFromToNumber('Category Id', 0, 2147483647)),
  query('size').toInt().notEmpty()
    .withMessage(GENERAL_ERRORS.isRequired('Size'))
    .isInt({ min: 0, max: 50 })
    .withMessage(GENERAL_ERRORS.isFromToNumber('Category Id', 0, 50)),
  validationResultMiddleware,
];

export const getOneValidator = [
  param('id').toInt(),
  validationResultMiddleware,
];

export const createValidator = [
  body('name').notEmpty().withMessage(GENERAL_ERRORS.isRequired('Name'))
    .isLength({ min: 2, max: 15 })
    .withMessage(GENERAL_ERRORS.fieldFromToString('Name', 2, 15))
    .matches(/^[A-Z]/)
    .withMessage(GENERAL_ERRORS.firstLetterUppercase),
  body('description').notEmpty().withMessage(GENERAL_ERRORS.isRequired('Description'))
    .isLength({ min: 2, max: 1000 })
    .withMessage(GENERAL_ERRORS.fieldFromToString('Description', 2, 1000))
    .matches(/^[A-Z]/)
    .withMessage(GENERAL_ERRORS.firstLetterUppercase),
  body('parentId').notEmpty().withMessage(GENERAL_ERRORS.isRequired('Category Id'))
    .isInt({ min: 0, max: 2147483647 })
    .withMessage(GENERAL_ERRORS.isFromToNumber('Parent Id', 0, 2147483647)),
  validationResultMiddleware,
];

export const updateValidator = [
  param('id').toInt(),
  body('name').optional().notEmpty().withMessage(GENERAL_ERRORS.isRequired('Name'))
    .isLength({ min: 2, max: 15 })
    .withMessage(GENERAL_ERRORS.fieldFromToString('Name', 2, 15))
    .matches(/^[a-zA-Z0-9_.-]*$/)
    .withMessage(GENERAL_ERRORS.firstLetterUppercase),
  body('description').optional().notEmpty().withMessage(GENERAL_ERRORS.isRequired('Description'))
    .isLength({ min: 2, max: 1000 })
    .withMessage(GENERAL_ERRORS.fieldFromToString('Description', 2, 1000))
    .matches(/^[a-zA-Z0-9_.-]*$/)
    .withMessage(GENERAL_ERRORS.firstLetterUppercase),
  validationResultMiddleware,
];

export const deleteValidator = [
  param('id').toInt(),
  validationResultMiddleware,
];
