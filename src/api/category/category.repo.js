import { RepositoryError } from '../../utils/error-handling.js';
import db from '../../services/db.js';

export const getQueryCategoriesRepo = async (query, attributes, include, where) => {
  try {
    return await db.Category.findAll({
      attributes,
      include,
      offset: query.from,
      limit: query.size,
      where,
    });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const getCategoriesRepo = async (attributes, include, where) => {
  try {
    return await db.Category.findAll({
      attributes,
      include,
      where,
      raw: true,
    });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const getHierarchicalCategoriesRepo = async (categoryId) => {
  try {
    const subcategoryQuery = `
            WITH RECURSIVE subcategory AS (
              SELECT id, name, description, "parentId"
              FROM category
              WHERE id = ${categoryId}
              
              UNION ALL
              
              SELECT c.id, c.name,c.description, c."parentId"
              FROM category c
              INNER JOIN subcategory s ON s.id = c."parentId"
            )
            SELECT *
            FROM subcategory;
       `;

    return await db.sequelize.query(subcategoryQuery, {
      type: db.sequelize.QueryTypes.SELECT,
    });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const getCategoryByIdRepo = async (id, attributes, include) => {
  try {
    return await db.Category.findOne({
      attributes,
      include,
      where: {
        id,
      },
    });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const getCategoryByNameRepo = async (name, attributes, include) => {
  try {
    return await db.Category.findOne({
      attributes,
      include,
      where: {
        name,
      },
    });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const createCategoryRepo = async (category) => {
  try {
    return await db.Category.create(category);
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const updateCategoryByIdRepo = async (id, category) => {
  try {
    return await db.Category.update(category, { where: { id } });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const deleteCategoryByIdRepo = async (id) => {
  try {
    return await db.Category.destroy({ where: { id } });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};
