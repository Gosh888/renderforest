export default (sequelize, DataTypes) => {
  const Category = sequelize.define(
    'category',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      description: {
        type: DataTypes.STRING,
        allowNull: false,
      },

    },
    {
      paranoid: true,
      timestamps: true,
      tableName: 'category',
    },
  );
  Category.associate = (models) => {
    models.Category.hasOne(models.Item);
    models.Category.belongsTo(models.Category, { foreignKey: 'parentId' });
  };
  return Category;
};
