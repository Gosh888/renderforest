import {
  deleteCategoryByIdService,
  getCategoryByIdOrFailService,
  getCategoriesService,
  createCategoryService,
  updateCategoryByIdService,
  getHierarchicalCategoriesService,
  getOneHierarchicalCategoriesService,
} from './category.service.js';
import db from '../../services/db.js';

export const getCategoriesController = async (req, res, next) => {
  try {
    const got = await getCategoriesService(req.query, null);
    res.send(got);
  } catch (err) {
    next(err);
  }
};

export const getHierarchicalCategoriesController = async (req, res, next) => {
  try {
    const got = await getHierarchicalCategoriesService();
    res.send(got);
  } catch (err) {
    next(err);
  }
};

export const getOneHierarchicalCategoriesController = async (req, res, next) => {
  try {
    const got = await getOneHierarchicalCategoriesService(req.params.id);
    res.send(got);
  } catch (err) {
    next(err);
  }
};

export const getCategoryByIdController = async (req, res, next) => {
  try {
    const got = await getCategoryByIdOrFailService(req.params.id, null, [db.Item]);
    res.send(got);
  } catch (err) {
    next(err);
  }
};

export const createCategoryController = async (req, res, next) => {
  try {
    const created = await createCategoryService(req.body);
    res.send(created);
  } catch (err) {
    next(err);
  }
};

export const updateCategoryByIdController = async (req, res, next) => {
  try {
    const updated = await updateCategoryByIdService(req.params.id, req.body);
    res.send(updated);
  } catch (err) {
    next(err);
  }
};

export const deleteCategoryByIdController = async (req, res, next) => {
  try {
    const deleted = await deleteCategoryByIdService(req.params.id);
    res.send(deleted);
  } catch (err) {
    next(err);
  }
};
