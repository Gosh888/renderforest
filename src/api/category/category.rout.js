import { Router } from 'express';
import {
  createCategoryController,
  deleteCategoryByIdController,
  getCategoriesController,
  getCategoryByIdController, getHierarchicalCategoriesController, getOneHierarchicalCategoriesController,
  updateCategoryByIdController,
} from './category.controller.js';
import {
  createValidator, deleteValidator, getOneValidator, updateValidator,
} from './category.validator.js';
import { adminAuthorization } from '../../middlewares/admin-authorization.js';

const router = Router();

router.get('/all/', getCategoriesController);

router.get('/one/:id', adminAuthorization, ...getOneValidator, getCategoryByIdController);

router.get('/all/hierarchical', getHierarchicalCategoriesController);

router.get('/one/hierarchical/:id', ...getOneValidator, getOneHierarchicalCategoriesController);

router.post('/', adminAuthorization, ...createValidator, createCategoryController);

router.put('/:id', adminAuthorization, ...updateValidator, updateCategoryByIdController);

router.delete('/:id', adminAuthorization, ...deleteValidator, deleteCategoryByIdController);

export default router;
