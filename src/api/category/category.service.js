import { ServiceError } from '../../utils/error-handling.js';
import {
  createCategoryRepo,
  deleteCategoryByIdRepo, getCategoriesRepo,
  getCategoryByIdRepo,
  getCategoryByNameRepo,
  getHierarchicalCategoriesRepo,
  getQueryCategoriesRepo,
  updateCategoryByIdRepo,
} from './category.repo.js';
import { CATEGORY_ERRORS, GENERAL_ERRORS } from '../../utils/error-messages.js';

// const generateCategoryTree = (categories, parentId = null) => {
//   const hierarchic = {};
//
//   categories
//     .filter((category) => category.parentId === parentId)
//     .forEach((category) => {
//       hierarchic[category.id] = { ...category, children: generateCategoryTree(categories, category.id) };
//     });
//
//   return hierarchic;
// }

const generateCategoryTree = (categories, parentId = null) => {
  const hierarchic = [];

  categories
    .filter((category) => category.parentId === parentId)
    .forEach((category) => {
      hierarchic.push({ ...category, children: generateCategoryTree(categories, category.id) });
    });

  return hierarchic;
};

export const getCategoriesService = async (query, attributes, include, where) => getQueryCategoriesRepo(query, attributes, include, where);

export const getHierarchicalCategoriesService = async () => {
  const hierarchicalCategories = await getCategoriesRepo();
  return generateCategoryTree(hierarchicalCategories, null);
};

export const getOneHierarchicalCategoriesService = async (id) => {
  const head = await getCategoryByIdOrFailService(id);
  const hierarchicalCategories = await getHierarchicalCategoriesRepo(head.id);
  return generateCategoryTree(hierarchicalCategories, id);
};

export const getCategoryByIdOrFailService = async (id, attributes, include) => {
  const got = await getCategoryByIdRepo(id, attributes, include);
  if (got == null) {
    throw new ServiceError(GENERAL_ERRORS.notFound('Category'), 404);
  }
  return got;
};

export const getCategoryByNameService = async (name, attributes, include) =>
  getCategoryByNameRepo(name, attributes, include);

export const getCategoryByNameOrFailService = async (name, attributes, include) => {
  const got = await getCategoryByNameRepo(name, attributes, include);
  if (got == null) {
    throw new ServiceError(GENERAL_ERRORS.notFound('Category'), 404);
  }
  return got;
};

export const createCategoryService = async (category) => {
  if (category.parentId != null) {
    await getCategoryByIdOrFailService(category.parentId);
  }

  const got = await getCategoryByNameService(category.name, ['id']);
  if (got != null) {
    throw new ServiceError(GENERAL_ERRORS.isExists('Name'), 403);
  }

  return createCategoryRepo(category);
};

export const updateCategoryByIdService = async (id, category) => {
  const got = await getCategoryByIdOrFailService(id);
  if (got.name === 'HEAD') {
    throw new ServiceError(CATEGORY_ERRORS.headCategoryFrozen, 403);
  }
  await updateCategoryByIdRepo(id, category);
  return { message: 'updated' };
};

export const deleteCategoryByIdService = async (id) => {
  const got = await getCategoryByIdOrFailService(id);
  if (got.name === 'HEAD') {
    throw new ServiceError(CATEGORY_ERRORS.headCategoryFrozen, 403);
  }
  await deleteCategoryByIdRepo(id);
  return { message: 'deleted' };
};
