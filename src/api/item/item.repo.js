import db from '../../services/db.js';
import { RepositoryError } from '../../utils/error-handling.js';
import {
  deleteElastic,
  indexElastic, matchAllElastic, matchByIdElastic, multiMatchElastic, updateElastic,
} from '../../services/elasticSearch.js';

// Sequelize

export const getItemsRepo = async (query, attributes, include) => {
  try {
    const { from, size, categoryId } = query;
    return await db.Item.findAll({
      attributes,
      include,
      offset: from,
      limit: size,
      where: {
        ...(categoryId && { categoryId }),
      },
    });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const getItemsByIdsRepo = async (ids, attributes, include) => {
  try {
    return await db.Item.findAll({
      attributes,
      include,
      where: {
        id: ids,
      },
    });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const getItemByIdRepo = async (id, attributes, include) => {
  try {
    return await db.Item.findOne({
      attributes,
      include,
      where: {
        id,
      },
    });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const createItemRepo = async (item) => {
  try {
    return await db.Item.create(item);
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const updateItemByIdRepo = async (id, item) => {
  try {
    return await db.Item.update(item, { where: { id } });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const deleteItemByIdRepo = async (id) => {
  try {
    return await db.Item.destroy({ where: { id } });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

// Elastic search

export const multiMatchElasticItemByQueryRepo = async (query, fields) => {
  try {
    const { body: { hits: { hits } } } = await multiMatchElastic('item', query.from, query.size, query.search, fields);
    return hits;
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const matchElasticItemByIdRepo = async (id) => {
  try {
    const { body: { hits: { hits } } } = await matchByIdElastic('item', id);
    return hits[0];
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const matchAllElasticItemRepo = async () => {
  try {
    const { body } = await matchAllElastic('item');
    return body;
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const updateElasticItemRepo = async (id, body) => {
  try {
    return updateElastic('item', id, body);
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const indexElasticItemRepo = async (body) => {
  try {
    return indexElastic('item', body);
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const deleteElasticItemRepo = async (id) => {
  try {
    return deleteElastic('item', id);
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};
