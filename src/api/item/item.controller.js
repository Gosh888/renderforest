import {
  createItemService,
  deleteItemByIdService, getElasticItemsService,
  getItemByIdOrFailService, getItemsByQueryService, getItemsService,
  updateItemByIdService,
} from './item.service.js';
import db from '../../services/db.js';

export const getElasticItemsController = async (req, res, next) => {
  try {
    const got = await getElasticItemsService();
    res.send(got);
  } catch (err) {
    next(err);
  }
};

export const getItemsController = async (req, res, next) => {
  try {
    const { search } = req.query;
    const attributes = null;
    const include = [db.File, db.Category];

    const got = await (search != null ? getItemsByQueryService(req.query, attributes, include)
      : getItemsService(req.query, attributes, include));
    res.send(got);
  } catch (err) {
    next(err);
  }
};

export const getItemByIdController = async (req, res, next) => {
  try {
    const got = await getItemByIdOrFailService(req.params.id);
    res.send(got);
  } catch (err) {
    next(err);
  }
};

export const createItemController = async (req, res, next) => {
  try {
    const created = await createItemService(req.body);
    res.send(created);
  } catch (err) {
    next(err);
  }
};

export const updateItemByIdController = async (req, res, next) => {
  try {
    const updated = await updateItemByIdService(req.params.id, req.body);
    res.send(updated);
  } catch (err) {
    next(err);
  }
};

export const deleteItemByIdController = async (req, res, next) => {
  try {
    const deleted = await deleteItemByIdService(req.params.id);
    res.send(deleted);
  } catch (err) {
    next(err);
  }
};
