import { ServiceError } from '../../utils/error-handling.js';
import {
  createItemRepo, deleteElasticItemRepo,
  deleteItemByIdRepo,
  getItemByIdRepo,
  getItemsByIdsRepo,
  getItemsRepo,
  indexElasticItemRepo,
  matchAllElasticItemRepo,
  matchElasticItemByIdRepo,
  multiMatchElasticItemByQueryRepo, updateElasticItemRepo,
  updateItemByIdRepo,
} from './item.repo.js';
import { GENERAL_ERRORS } from '../../utils/error-messages.js';
import { updateFileByIdService } from '../file/file.service.js';
import { getCategoryByIdOrFailService } from '../category/category.service.js';

export const getItemsService = async (query, attributes, include) => getItemsRepo(query, attributes, include);

export const getElasticItemsService = async () => matchAllElasticItemRepo();

export const getElasticItemOrFailService = async (id) => {
  const got = await matchElasticItemByIdRepo(id);
  if (got == null) {
    throw new ServiceError(GENERAL_ERRORS.notFound('Elastic Item'), 404);
  }
  return got;
};

export const getItemsByQueryService = async (query, attributes, include) => {
  const elasticGot = await multiMatchElasticItemByQueryRepo(query, ['tags', 'name', 'description']);
  const ids = elasticGot.map((e) => e._source.id);
  return getItemsByIdsRepo(ids, attributes, include);
};

export const getItemByIdOrFailService = async (id, attributes, include) => {
  const got = await getItemByIdRepo(id, attributes, include);
  if (got == null) {
    throw new ServiceError(GENERAL_ERRORS.notFound('Item'), 404);
  }
  return got;
};

export const createItemService = async (item) => {
  const {
    fileIds, ...rest
  } = item;
  await getCategoryByIdOrFailService(rest.categoryId);
  const created = await createItemRepo(rest);
  await Promise.all(fileIds.map((fi) => updateFileByIdService(fi, { itemId: created.id })));
  await indexElasticItemRepo({
    id: created.id, ...rest,
  });
  return created;
};

export const updateItemByIdService = async (id, item) => {
  const [elasticGot] = await Promise.all([
    getElasticItemOrFailService(id),
    getItemByIdOrFailService(id)]);

  await updateElasticItemRepo(elasticGot._id, item);
  await updateItemByIdRepo(id, item);
  return { message: 'updated' };
};

export const deleteItemByIdService = async (id) => {
  const [elasticGot] = await Promise.all([
    getElasticItemOrFailService(id),
    getItemByIdOrFailService(id)]);

  await deleteElasticItemRepo(elasticGot._id);
  await deleteItemByIdRepo(id);
  return { message: 'deleted' };
};
