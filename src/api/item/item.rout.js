import { Router } from 'express';
import {
  createItemController,
  deleteItemByIdController,
  getItemByIdController,
  getItemsController,
  updateItemByIdController,
} from './item.controller.js';
import {
  createValidator, deleteValidator, getAllValidator, getOneValidator, updateValidator,
} from './item.validator.js';
import { adminAuthorization } from '../../middlewares/admin-authorization.js';

const router = Router();

router.get('/', ...getAllValidator, getItemsController);

router.get('/:id', ...getOneValidator, getItemByIdController);

router.post('/', adminAuthorization, ...createValidator, createItemController);

router.put('/:id', adminAuthorization, ...updateValidator, updateItemByIdController);

router.delete('/:id', adminAuthorization, ...deleteValidator, deleteItemByIdController);

export default router;
