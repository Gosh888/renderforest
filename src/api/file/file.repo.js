import db from '../../services/db.js';
import { RepositoryError } from '../../utils/error-handling.js';

export const getFilesRepo = async (query, attributes, include) => {
  try {
    return await db.File.findAll({
      attributes, include, offset: query.from, limit: query.size,
    });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const getFileByIdRepo = async (id, attributes, include) => {
  try {
    return await db.File.findOne({
      attributes,
      include,
      where: {
        id,
      },
    });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const bulkCreateFileRepo = async (files) => {
  try {
    return await db.File.bulkCreate(files);
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const createFileRepo = async (file) => {
  try {
    return await db.File.create(file);
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const updateFileByIdRepo = async (id, file) => {
  try {
    return await db.File.update(file, { where: { id } });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const deleteFileByIdRepo = async (id) => {
  try {
    return await db.File.destroy({ where: { id } });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};
