import { Router } from 'express';
import multer from 'multer';
import { extname } from 'path';
import {
  bulkCreateFileController,
  createFileController,
  deleteFileByIdController,
  getFileByIdController,
  getFilesController,
} from './file.controller.js';
import { deleteValidator, getOneValidator } from './file.validator.js';
import { adminAuthorization } from '../../middlewares/admin-authorization.js';
import { ValidatorError } from '../../utils/error-handling.js';

const multiStorage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, 'uploads');
  },
  filename(req, file, cb) {
    cb(null, `${Date.now()}${file.originalname.replace(/ /g, '')}`);
  },
});

const fileFilter = (req, file, cb) => {
  // Allowed ext
  const filetypes = /jpeg|jpg|png|gif/;
  // Check ext
  const extnameCheck = filetypes.test(extname(file.originalname).toLowerCase());
  // Check mime
  const mimetype = filetypes.test(file.mimetype);

  if (mimetype && extnameCheck) {
    return cb(null, true);
  }
  cb(new ValidatorError('Images Only!'));
};

const multiUpload = multer({ storage: multiStorage });

const singleStorage = multer.memoryStorage();
const singleUpload = multer({ storage: singleStorage, fileFilter });

const router = Router();

router.get('/', adminAuthorization, getFilesController);

router.get('/:id', adminAuthorization, ...getOneValidator, getFileByIdController);

router.post('/many/', adminAuthorization, multiUpload.array('files'), bulkCreateFileController);

router.post('/one/', adminAuthorization, singleUpload.single('file'), createFileController);

router.delete('/:id', adminAuthorization, ...deleteValidator, deleteFileByIdController);

export default router;
