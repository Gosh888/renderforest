import {
  bulkCreateFileService, createFileService,
  deleteFileByIdService,
  getFileByIdOrFailService,
  getFilesService,
} from './file.service.js';

export const getFilesController = async (req, res, next) => {
  try {
    const got = await getFilesService(req.query);
    res.send(got);
  } catch (err) {
    next(err);
  }
};

export const getFileByIdController = async (req, res, next) => {
  try {
    const got = await getFileByIdOrFailService(req.params.id);
    res.send(got);
  } catch (err) {
    next(err);
  }
};

export const bulkCreateFileController = async (req, res, next) => {
  try {
    const created = await bulkCreateFileService(req.files);
    res.send(created);
  } catch (err) {
    next(err);
  }
};

export const createFileController = async (req, res, next) => {
  try {
    const created = await createFileService(req.file);
    res.send(created);
  } catch (err) {
    next(err);
  }
};

export const deleteFileByIdController = async (req, res, next) => {
  try {
    const deleted = await deleteFileByIdService(req.params.id);
    res.send(deleted);
  } catch (err) {
    next(err);
  }
};
