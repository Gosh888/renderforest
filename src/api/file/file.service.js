import sharp from 'sharp';
import { ServiceError } from '../../utils/error-handling.js';
import {
  bulkCreateFileRepo, createFileRepo,
  deleteFileByIdRepo,
  getFileByIdRepo,
  getFilesRepo,
  updateFileByIdRepo,
} from './file.repo.js';
import { GENERAL_ERRORS } from '../../utils/error-messages.js';

export const getFilesService = async (query, attributes, include) => getFilesRepo(query, attributes, include);

export const getFileByIdOrFailService = async (id, attributes, include) => {
  const got = await getFileByIdRepo(id, attributes, include);
  if (got == null) {
    throw new ServiceError(GENERAL_ERRORS.notFound('File'), 404);
  }
  return got;
};

export const bulkCreateFileService = async (files) => {
  const created = await bulkCreateFileRepo(files);
  return { fileIds: created.map((c) => c.id) };
};

// {
//   fieldname: 'files',
//       originalname: 'photo-1570693148754-5990bc28c97f.jpeg',
//     encoding: '7bit',
//     mimetype: 'image/jpeg',
//     destination: 'uploads',
//     filename: '1683120609119photo-1570693148754-5990bc28c97f.jpeg',
//     path: 'uploads/1683120609119photo-1570693148754-5990bc28c97f.jpeg',
//     size: 179196
// }

// file {
//   fieldname: 'file',
//       originalname: 'photo-1570693148754-5990bc28c97f.jpeg',
//       encoding: '7bit',
//       mimetype: 'image/jpeg',
//       buffer: <Buffer ff d8 ff e0 00 10 4a 46 49 46 00 01 01 01 00 48 00 48 00 00 ff e2 0c 58 49 43 43 5f 50 52 4f 46 49 4c 45 00 01 01 00 00 0c 48 4c 69 6e 6f 02 10 00 00 ... 179146 more bytes>,
//       size: 179196
// }

export const createFileService = async (file) => {
  const { buffer, ...rest } = file;
  const destination = 'uploads';
  const filename = `${Date.now()}${file.originalname.replace(/ /g, '')}`;
  const path = `${destination}/${filename}`;
  const width = 2000;
  const height = 1000;

  const resize = await sharp(buffer)
    .resize(width, height, {
      fit: 'inside',
    }).toBuffer();

  await sharp({
    create: {
      width,
      height,
      channels: 4,
      background: {
        r: 111, g: 115, b: 120, alpha: 1,
      },
    },
  })
    .composite([{ input: resize, gravity: 'center' }])
    .jpeg()
    .toFile(path);

  return createFileRepo({
    ...rest, destination, filename, path,
  });
};

export const updateFileByIdService = async (id, file) => {
  await updateFileByIdRepo(id, file);
  return { message: 'updated' };
};

export const deleteFileByIdService = async (id) => {
  await getFileByIdOrFailService(id);
  await deleteFileByIdRepo(id);
  return { message: 'deleted' };
};
