import { param, query } from 'express-validator';
import { validationResultMiddleware } from '../../middlewares/validation-result.js';
import { GENERAL_ERRORS } from '../../utils/error-messages.js';

export const getAllValidator = [
  query('from').toInt().notEmpty().withMessage(GENERAL_ERRORS.isRequired('From'))
    .isInt({ min: 0, max: 2147483647 })
    .withMessage(GENERAL_ERRORS.isFromToNumber('Category Id', 0, 2147483647)),
  query('size').toInt().notEmpty()
    .withMessage(GENERAL_ERRORS.isRequired('Size'))
    .isInt({ min: 0, max: 50 })
    .withMessage(GENERAL_ERRORS.isFromToNumber('Category Id', 0, 50)),
  validationResultMiddleware,
];

export const getOneValidator = [
  param('id').toInt(),
  validationResultMiddleware,
];

export const deleteValidator = [
  param('id').toInt(),
  validationResultMiddleware,
];
