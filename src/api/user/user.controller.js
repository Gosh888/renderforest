import {
  createAdminService,
  deleteUserByIdService,
  getUserByIdOrFailService,
  getUsersService,
  updateUserByIdService,
} from './user.service.js';

export const getUsersController = async (req, res, next) => {
  try {
    const got = await getUsersService(req.query);
    res.send(got);
  } catch (err) {
    next(err);
  }
};

export const getUserByIdController = async (req, res, next) => {
  try {
    const got = await getUserByIdOrFailService(req.params.id);
    res.send(got);
  } catch (err) {
    next(err);
  }
};

export const createUserController = async (req, res, next) => {
  try {
    const created = await createAdminService(req.body);
    res.send(created);
  } catch (err) {
    next(err);
  }
};

export const updateUserByIdController = async (req, res, next) => {
  try {
    const updated = await updateUserByIdService(req.params.id, req.body);
    res.send(updated);
  } catch (err) {
    next(err);
  }
};

export const deleteUserByIdController = async (req, res, next) => {
  try {
    const deleted = await deleteUserByIdService(req.params.id);
    res.send(deleted);
  } catch (err) {
    next(err);
  }
};
