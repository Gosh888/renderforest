import { Router } from 'express';
import {
  createUserController,
  deleteUserByIdController,
  getUserByIdController,
  getUsersController,
  updateUserByIdController,
} from './user.controller.js';
import {
  createValidator, deleteValidator, getAllValidator, getOneValidator, updateValidator,
} from './user.validator.js';
import { adminAuthorization } from '../../middlewares/admin-authorization.js';

const router = Router();

router.get('/', adminAuthorization, ...getAllValidator, getUsersController);

router.get('/:id', adminAuthorization, ...getOneValidator, getUserByIdController);

router.post('/', adminAuthorization, ...createValidator, createUserController);

router.put('/:id', adminAuthorization, ...updateValidator, updateUserByIdController);

router.delete('/:id', adminAuthorization, ...deleteValidator, deleteUserByIdController);

export default router;
