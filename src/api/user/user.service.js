import { ServiceError } from '../../utils/error-handling.js';
import {
  createUserRepo,
  deleteUserByIdRepo,
  getUserByEmailRepo,
  getUserByIdRepo,
  getUserByRoleIdRepo,
  getUsersRepo,
  updateUserByIdRepo,
} from './user.repo.js';
import { hashPassword } from '../../services/bcrypt.js';
import { GENERAL_ERRORS, USER_ERRORS } from '../../utils/error-messages.js';
import { getRoleByIdOrFailService, getRoleByNameService } from '../role/role.service.js';
import db from '../../services/db.js';

export const getUsersService = async (query, attributes, include) => getUsersRepo(query, attributes, include);

export const getUserByIdOrFailService = async (id, attributes, include) => {
  const got = await getUserByIdRepo(id, attributes, include);
  if (got == null) {
    throw new ServiceError(GENERAL_ERRORS.notFound('User'), 404);
  }
  return got;
};

export const getUserByRoleIdService = async (roleId, attributes, include) => getUserByRoleIdRepo(roleId, attributes, include);

export const getUserByEmailOrFailService = async (email, attributes, include) => {
  const got = await getUserByEmailRepo(email, attributes, include);
  if (got == null) {
    throw new ServiceError(GENERAL_ERRORS.notFound('User'), 404);
  }
  return got;
};

export const getUserByEmailService = async (email, attributes, include) => getUserByEmailRepo(email, attributes, include);

export const createUserService = async (user) => {
  const [got, role] = await Promise.all([
    getUserByEmailService(user.email, ['id']),
    getRoleByNameService('CLIENT'), ['id']]);

  if (got != null) {
    throw new ServiceError(GENERAL_ERRORS.isExists('Email'), 403);
  }
  const password = hashPassword(user.password);

  return createUserRepo({
    ...user,
    password,
    roleId: role.id,
  });
};

export const createAdminService = async (user, roleId) => {
  const [got, role] = await Promise.all([
    getUserByEmailService(user.email, ['id']),
    roleId == null ? getRoleByNameService('ADMIN')
      : getRoleByIdOrFailService(roleId, ['id'])]);

  if (got != null) {
    throw new ServiceError(GENERAL_ERRORS.isExists('Email'), 403);
  }
  const password = hashPassword(user.password);

  return createUserRepo({
    ...user,
    password,
    roleId: role.id,
  });
};

export const updateUserByIdService = async (id, user) => {
  const got = await getUserByIdOrFailService(id, null, [db.Role]);
  if (got.role.name === 'SUPER ADMIN') {
    throw new ServiceError(USER_ERRORS.superAdminFrozen, 403);
  }
  await updateUserByIdRepo(id, user);
  return { message: 'updated' };
};

export const deleteUserByIdService = async (id) => {
  const got = await getUserByIdOrFailService(id, null, [db.Role]);
  if (got.role.name === 'SUPER ADMIN') {
    throw new ServiceError(USER_ERRORS.superAdminFrozen, 403);
  }
  await deleteUserByIdRepo(id);
  return { message: 'deleted' };
};
