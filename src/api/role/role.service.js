import { ServiceError } from '../../utils/error-handling.js';
import {
  createRoleRepo,
  deleteRoleByIdRepo,
  getRoleByIdRepo,
  getRoleByNameRepo,
  getRolesRepo,
  updateRoleByIdRepo,
} from './role.repo.js';
import { GENERAL_ERRORS } from '../../utils/error-messages.js';

export const getRolesService = async (query, attributes, include) => getRolesRepo(query, attributes, include);

export const getRoleByIdOrFailService = async (id, attributes, include) => {
  const got = await getRoleByIdRepo(id, attributes, include);
  if (got == null) {
    throw new ServiceError(GENERAL_ERRORS.notFound('Role'), 404);
  }
  return got;
};

export const getRoleByNameService = async (name, attributes, include) => getRoleByNameRepo(name, attributes, include);

export const createRoleService = async (role) => {
  const got = await getRoleByNameService(role.name, ['id']);
  if (got != null) {
    throw new ServiceError(GENERAL_ERRORS.isExists('Name'), 403);
  }

  return createRoleRepo(role);
};

export const updateRoleByIdService = async (id, role) => {
  await getRoleByIdOrFailService(id);
  await updateRoleByIdRepo(id, role);
  return { message: 'updated' };
};

export const deleteRoleByIdService = async (id) => {
  await getRoleByIdOrFailService(id);
  await deleteRoleByIdRepo(id);
  return { message: 'deleted' };
};
