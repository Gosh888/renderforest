import db from '../../services/db.js';
import { RepositoryError } from '../../utils/error-handling.js';

export const getRolesRepo = async (query, attributes, include) => {
  try {
    return await db.Role.findAll({
      attributes, include, offset: query.from, limit: query.size,
    });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const getRoleByIdRepo = async (id, attributes, include) => {
  try {
    return await db.Role.findOne({
      attributes,
      include,
      where: {
        id,
      },
    });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const getRoleByNameRepo = async (name, attributes, include) => {
  try {
    return await db.Role.findOne({
      attributes,
      include,
      where: {
        name,
      },
    });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const createRoleRepo = async (role) => {
  try {
    return await db.Role.create(role);
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const updateRoleByIdRepo = async (id, role) => {
  try {
    return await db.Role.update(role, { where: { id } });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};

export const deleteRoleByIdRepo = async (id) => {
  try {
    return await db.Role.destroy({ where: { id } });
  } catch (err) {
    throw new RepositoryError(err.message, 500);
  }
};
