import { Router } from 'express';
import {
  createRoleController,
  deleteRoleByIdController,
  getRoleByIdController,
  getRolesController,
  updateRoleByIdController,
} from './role.controller.js';
import {
  createValidator, deleteValidator, getOneValidator, updateValidator,
} from './role.validator.js';
import { adminAuthorization } from '../../middlewares/admin-authorization.js';
import { getAllValidator } from '../category/category.validator.js';

const router = Router();

router.get('/', adminAuthorization, ...getAllValidator, getRolesController);

router.get('/:id', adminAuthorization, ...getOneValidator, getRoleByIdController);

router.post('/', adminAuthorization, ...createValidator, createRoleController);

router.put('/:id', adminAuthorization, ...updateValidator, updateRoleByIdController);

router.delete('/:id', adminAuthorization, ...deleteValidator, deleteRoleByIdController);

export default router;
