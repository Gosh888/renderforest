import {
  createRoleService,
  deleteRoleByIdService,
  getRoleByIdOrFailService,
  getRolesService,
  updateRoleByIdService,
} from './role.service.js';

export const getRolesController = async (req, res, next) => {
  try {
    const got = await getRolesService(req.query);
    res.send(got);
  } catch (err) {
    next(err);
  }
};

export const getRoleByIdController = async (req, res, next) => {
  try {
    const got = await getRoleByIdOrFailService(req.params.id);
    res.send(got);
  } catch (err) {
    next(err);
  }
};

export const createRoleController = async (req, res, next) => {
  try {
    const created = await createRoleService(req.body);
    res.send(created);
  } catch (err) {
    next(err);
  }
};

export const updateRoleByIdController = async (req, res, next) => {
  try {
    const updated = await updateRoleByIdService(req.params.id, req.body);
    res.send(updated);
  } catch (err) {
    next(err);
  }
};

export const deleteRoleByIdController = async (req, res, next) => {
  try {
    const deleted = await deleteRoleByIdService(req.params.id);
    res.send(deleted);
  } catch (err) {
    next(err);
  }
};
