import Sequelize from 'sequelize';

import role from '../api/role/role.entity.js';
import user from '../api/user/user.entity.js';
import category from '../api/category/category.entity.js';
import item from '../api/item/item.entity.js';
import file from '../api/file/file.entity.js';

const sequelize = new Sequelize('postgres', process.env.POSTGRES_USER, process.env.POSTGRES_PASSWORD, {
  host: process.env.POSTGRES_HOST,
  dialect: 'postgres',
  operatorsAliases: 0,
  logging: false,
  pool: {
    max: 5,
    min: 0,
    acquire: 3000,
    idle: 10000,
  },
});

const database = await sequelize.query(`SELECT 1 FROM pg_database WHERE datname = '${process.env.POSTGRES_DB}'`);
if (database[0].length === 0) { // if the database does not exist
  // create the database
  await sequelize.query(`CREATE DATABASE ${process.env.POSTGRES_DB}`);
} else {
  console.log(`Database "${process.env.POSTGRES_DB}" already exists`);
}

const db = {};

db.Role = role(sequelize, Sequelize);
db.User = user(sequelize, Sequelize);
db.Category = category(sequelize, Sequelize);
db.Item = item(sequelize, Sequelize);
db.File = file(sequelize, Sequelize);

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

await sequelize.sync({ force: false, alter: true });
// await sequelize.sync({ force: true });

export default db;
