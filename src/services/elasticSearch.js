import { Client } from '@elastic/elasticsearch';

export const dbElasticSearchConfig = async () => {
  const elasticURL = `http://${process.env.ELASTIC_SEARCH_HOST}:${process.env.ELASTIC_SEARCH_PORT}`;
  global.elasticClient = new Client({ node: elasticURL });
};

export const multiMatchElastic = (index, from, size, query, fields) => global.elasticClient.search({
  index,
  body: {
    from,
    size,
    query: {
      multi_match: {
        query,
        fields,
      },
    },
  },
});

export const matchByIdElastic = (index, id) => global.elasticClient.search({
  index,
  body: {
    query: {
      match: {
        id,
      },
    },
  },
});

export const matchAllElastic = (index) => global.elasticClient.search({
  index,
  body: {
    query: {
      match_all: {},
    },
  },
});

export const indexElastic = (index, body) => global.elasticClient.index({ index, refresh: true, body });

export const updateElastic = (index, id, doc) => {
  global.elasticClient.update({
    index,
    id,
    body: {
      doc,
    },
  });
};

export const deleteElastic = (index, id) => global.elasticClient.delete({ index, id });
