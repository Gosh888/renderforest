import express from 'express';
import userRouter from './api/user/user.rout.js';
import authRouter from './api/auth/auth.rout.js';
import categoryRouter from './api/category/category.rout.js';
import roleRouter from './api/role/role.rout.js';
import itemRouter from './api/item/item.rout.js';
import fileRouter from './api/file/file.rout.js';
import { initProject } from './init.js';

await initProject();

export const app = express();
app.use(express.json());
app.use(express.static('./uploads'));

app.use('/auth', authRouter);
app.use('/users', userRouter);
app.use('/categories', categoryRouter);
app.use('/roles', roleRouter);
app.use('/items', itemRouter);
app.use('/files', fileRouter);

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  try {
    console.log('error___', err);
    res.status(err.statusCode);
    res.send({
      errors: err.errors,
    });
  } catch {
    res.send({
      err,
    });
  }
});
