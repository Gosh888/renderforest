## Description

Project with Elastic search

## Starting dev mode commands

```bash
$ docker-compose up
```

## Installation

```bash
$ npm i -g pnpm
$ pnpm i
```

## Running the app

```bash
# development
$ pnpm start

```
